package accion.com.webview_login_linkedin

import accion.linkedinLogin.common.URLManager.*
import accion.linkedinLogin.control.LinkedInControl
import accion.linkedinLogin.dto.*
import accion.linkedinLogin.events.LinkedInAccessTokenResponse
import accion.linkedinLogin.events.LinkedInEmailAddressResponse
import accion.linkedinLogin.events.LinkedInProfileDataResponse
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    var dialog: Dialog? = null

    private val MODE_EMAIL_ADDRESS_ONLY = 0
    private val MODE_LITE_PROFILE_ONLY = 1
    private val MODE_BOTH_OPTIONS = 2

    private val SCOPE_LITE_PROFILE = "r_liteprofile"
    private val SCOPE_EMAIL_ADDRESS = "r_emailaddress"

    private val clientID: String? = "81z54lxw7zxbik"
    private val clientSecret: String? = "x76HAdBJxQiPWY3Z"
    private val redirectionURL: String? = "https://www.accionlabs.com"

    private val linkedInUserProfile = LinkedInUserProfile()
    private val linkedInEmailAddress = LinkedInEmailAddress()
    private val linkedInUser = LinkedInUser()
    private var mode = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener(View.OnClickListener {
            showAuthenticateView(MODE_BOTH_OPTIONS)
        })
    }

    private fun showAuthenticateView(mode: Int) {
        this.mode = mode
        var URL: String =
            URL_FOR_AUTHORIZATION_CODE + "?response_type=code&client_id=" + clientID + "&redirect_uri=" + redirectionURL + "&state=aRandomString&scope="
        when (mode) {
            MODE_EMAIL_ADDRESS_ONLY -> URL += SCOPE_EMAIL_ADDRESS
            MODE_LITE_PROFILE_ONLY -> URL += SCOPE_LITE_PROFILE
            MODE_BOTH_OPTIONS -> URL += SCOPE_LITE_PROFILE + "," + SCOPE_EMAIL_ADDRESS
            else -> URL += SCOPE_EMAIL_ADDRESS
        }
        dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.linkedin_popup_layout)
        val window: Window = dialog!!.getWindow()!!
        window.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        val linkedInWebView: WebView = dialog!!.findViewById(R.id.linkedInWebView)
        val settings = linkedInWebView.settings
        try {
            settings.allowFileAccess = false
        } catch (ignored: Exception) {
        }
        linkedInWebView.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        try {
            linkedInWebView.settings.allowFileAccess = false
        } catch (ignored: Exception) {
        }
        linkedInWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return if (url.startsWith(redirectionURL + "/?code")) {
                    val newString: String =
                        url.replace(redirectionURL + "/?code=", "").trim({ it <= ' ' })
                    val code =
                        newString.substring(0, newString.indexOf("&state=aRandomString"))
                    getAccessToken(code)

                    true
                } else {

                    false
                }
            }
        }
        linkedInWebView.loadUrl(URL)
        dialog!!.show()
    }

    private fun getAccessToken(code: String) {
        val accessTokenURL: String = (URL_FOR_ACCESS_TOKEN.toString() + "?"
                + "client_id=" + clientID
                + "&client_secret=" + clientSecret
                + "&grant_type=authorization_code"
                + "&redirect_uri=" + redirectionURL
                + "&code=" + code)
        LinkedInControl().getAccessToken(accessTokenURL, object : LinkedInAccessTokenResponse {
            override fun onAuthenticationSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val linkedInAccessToken: LinkedInAccessToken = gson.fromJson<LinkedInAccessToken>(
                    jsonObject.toString(),
                    LinkedInAccessToken::class.java
                )
                getRelevantData(linkedInAccessToken)
            }

            override fun onAuthenticationFailed() {

            }
        })
    }

    private fun getLinkedInProfileData(linkedInAccessToken: LinkedInAccessToken): LinkedInUserProfile? {
        LinkedInControl().getProfileData(
            URL_FOR_PROFILE_DATA + linkedInAccessToken.getAccess_token(),
            object : LinkedInProfileDataResponse {
                override fun onRequestSuccess(jsonObject: JSONObject) {
                    try {
                        val gson = Gson()
                        val linkedInNameBean: LinkedInNameBean = gson.fromJson<LinkedInNameBean>(
                            jsonObject.toString(),
                            LinkedInNameBean::class.java
                        )
                        linkedInUserProfile.setUserName(linkedInNameBean)
                        Log.i("firstName ====> ", "" + linkedInNameBean.firstName)
                        try {
                            val imageURL = jsonObject
                                .getJSONObject("profilePicture")
                                .getJSONObject("displayImage~")
                                .getJSONArray("elements")
                                .getJSONObject(3)
                                .getJSONArray("identifiers")
                                .getJSONObject(0)
                                .getString("identifier")
                            linkedInUserProfile.setImageURL(imageURL)
                            Log.i("imageURL ====> ", imageURL)
                        } catch (ignored: java.lang.Exception) {
                            linkedInUserProfile.setImageURL("")
                        }
                        dialog!!.dismiss()
                    } catch (ignored: java.lang.Exception) {
                        dialog!!.dismiss()
                    }
                }

                override fun onRequestFailed() {
                    dialog!!.dismiss()
                }
            })
        return linkedInUserProfile
    }

    private fun getLinkedInEmailAddress(linkedInAccessToken: LinkedInAccessToken): LinkedInEmailAddress? {
        LinkedInControl().getEmailAddress(
            URL_FOR_EMAIL_ADDRESS + linkedInAccessToken.getAccess_token(),
            object : LinkedInEmailAddressResponse {
                override fun onSuccessResponse(jsonObject: JSONObject) {
                    try {
                        val emailAddress = jsonObject
                            .getJSONArray("elements")
                            .getJSONObject(0)
                            .getJSONObject("handle~")
                            .getString("emailAddress")
                        linkedInEmailAddress.setEmailAddress(emailAddress)
                        Log.i("emailAddress ==> ", emailAddress)
                        dialog!!.dismiss()
                    } catch (ignored: java.lang.Exception) {
                        linkedInEmailAddress.setEmailAddress("")
                        dialog!!.dismiss()
                    }
                }

                override fun onFailedResponse() {
                    dialog!!.dismiss()
                }
            })
        return linkedInEmailAddress
    }

    private fun getRelevantData(linkedInAccessToken: LinkedInAccessToken): LinkedInUser? {
        when (mode) {
            MODE_EMAIL_ADDRESS_ONLY -> linkedInUser.setLinkedInEmailAddress(
                getLinkedInEmailAddress(linkedInAccessToken)
            )
            MODE_LITE_PROFILE_ONLY -> linkedInUser.setUserProfile(
                getLinkedInProfileData(linkedInAccessToken)
            )
            MODE_BOTH_OPTIONS -> {
                linkedInUser.setLinkedInEmailAddress(getLinkedInEmailAddress(linkedInAccessToken))
                linkedInUser.setUserProfile(getLinkedInProfileData(linkedInAccessToken))
            }
            else -> linkedInUser.setLinkedInEmailAddress(getLinkedInEmailAddress(linkedInAccessToken))
        }
        return linkedInUser
    }
}