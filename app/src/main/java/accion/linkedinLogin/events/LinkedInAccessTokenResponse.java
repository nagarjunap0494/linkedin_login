package accion.linkedinLogin.events;

import org.json.JSONObject;

public interface LinkedInAccessTokenResponse {
    void onAuthenticationSuccess(JSONObject jsonObject);
    void onAuthenticationFailed();
}
